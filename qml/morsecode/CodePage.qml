// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import "morse.js" as Morse

Page {
    GridView {
        anchors {
            fill: parent
            leftMargin: 25
        }

        cellWidth: 110
        model: model
        delegate: Rectangle {
            width: 100
            height: 90
            //color: "#757575"
            color: theme.selectionColor
            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: 5
                }
                font.pixelSize: 30
                color: "white"
                text: key
            }
            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    bottom: parent.bottom
                    bottomMargin: 5
                }
                font.pixelSize: 45
                font.bold: true
                color: "white"
                text: code
            }
        }
    }

    ListModel {
        id: model
    }

    Component.onCompleted: {
        var codes = Object.keys(Morse.CODES)
        for (var i = 0; i < codes.length; i++) {
            var code = codes[i]
            var key = Morse.CODES[code]
            model.append({ key: key, code: code })
        }
    }
}
