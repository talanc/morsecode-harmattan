import QtQuick 1.1

Item {
    id: backspace

    width: 80
    height: 80

    property int holdTime: 500
    property int repeatInterval: 50

    signal pressed()

    Image {
        anchors {
            bottom: parent.bottom
            right: parent.right
        }

        width: 60
        height: 60

        source: "image://theme/icon-m-telephony-backspace" +
                (mouseArea.pressed ? "-selected" : "")
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onPressed: {
            backspace.pressed()
            backspaceTimer.interval = holdTime
            backspaceTimer.start()
        }
        onReleased: {
            backspaceTimer.stop()
        }


        Timer {
            id: backspaceTimer
            repeat: true

            onTriggered: {
                interval = repeatInterval
                backspace.pressed()
            }
        }
    }
}
