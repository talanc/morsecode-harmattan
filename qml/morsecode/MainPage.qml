import QtQuick 1.1
import com.nokia.meego 1.0
import "morse.js" as Morse

Page {

    property string input: ""
    property string output: ""
    property string underscore: "_"

    Rectangle {
        id: bg

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: controls.top
            margins: 20
        }

        color: "black"

        Timer {
            id: colorTimer
            interval: 100
            onTriggered: parent.color = "black"
        }

        function err() {
            color = "#552222"
            colorTimer.start()
        }

        Label {
            id: label
            anchors.fill: parent

            horizontalAlignment: Text.AlignHCenter
            //horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter

            font.pixelSize: 50

            text: (input === "" ? output : output + " " + input) + underscore
            color: theme.selectionColor
        }

        BackspaceButton {
            anchors {
                bottom: parent.bottom
                right: parent.right
            }

            onPressed: {
                if (input.length === 0) {
                    if (output.length > 0) {
                        output = output.substring(0, output.length - 1)
                    }
                } else {
                    input = input.substring(0, input.length - 1)
                }
            }
        }

        Rectangle {
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            height: 1
            color: "#313131"
        }
    }

    Column {
        id: controls

        spacing: 15

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 40
        }

        Row {
            spacing: 15

            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            function morseButtonPressed(code) {
                input += code
            }

            MorseButton {
                text: "&ndash;"
                code: Morse.CODE_DASH
                onPressed: parent.morseButtonPressed(code)
            }

            MorseButton {
                text: "."
                code: Morse.CODE_DOT
                onPressed: parent.morseButtonPressed(code)
            }
        }

        MorseButton {
            width: 195
            height: 70
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
            text: "/"
            baseColor: theme.selectionColor
            onPressed: {
                if (input === "") {
                    if (output.length > 0) {
                        var lc = output[output.length - 1]
                        if (lc === " ") {
                            output = output.substring(0, output.length - 1) + "\n"
                        } else if (lc !== "\n") {
                            output += " "
                        }
                    }
                } else {
                    var tr = Morse.CODES[input]
                    if (tr) {
                        output += tr
                        input = ""
                    } else {
                        bg.err()
                    }
                }
            }
        }
    }
}
