import QtQuick 1.1

Rectangle {
    property alias text: txt.text
    property alias textColor: txt.color

    property string code: ""

    signal pressed(string code)

    width: 90
    height: 90

    property color baseColor: "#757575"
    color: mouseArea.pressed ? Qt.lighter(baseColor) : baseColor

    Text {
        id: txt
        anchors.centerIn: parent
        color: "white"

        font {
            pixelSize: Math.min(parent.width, parent.height) * 0.5
        }

        text: "-"
        textFormat: Text.RichText
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onPressed: parent.pressed(parent.code)
    }
}
