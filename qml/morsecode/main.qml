import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
    id: appWindow

    initialPage: page

    Page {
        id: page

        TabGroup {
            currentTab: mainTab

            MainPage {
                id: mainTab
                orientationLock: PageOrientation.LockPortrait
            }
            CodePage {
                id: codeTab
                orientationLock: PageOrientation.LockPortrait
            }
            SettingsPage {
                id: settingsTab
                orientationLock: PageOrientation.LockPortrait
            }
        }

        tools: ToolBarLayout {
            ButtonRow {
                TabButton {
                    iconSource: "image://theme/icon-m-toolbar-home" +
                                (theme.inverted ? "-white" : "")
                    tab: mainTab
                }

                TabButton {
                    iconSource: "image://theme/icon-m-toolbar-list" +
                                (theme.inverted ? "-white" : "")
                    tab: codeTab
                }
                TabButton {
                    iconSource: "image://theme/icon-m-toolbar-settings" +
                                (theme.inverted ? "-white" : "")
                    tab: settingsTab
                }
            }
        }
    }

    Component.onCompleted: {
        theme.inverted = true
        theme.colorScheme = "16"
    }
}
